<?php
  /*** Helpers ***/
//asdasdsad was ist das
  function js_log($data) {
    $val = null;
    if (is_array($data) || is_object($data)) {
      $val = json_encode($data);
    } else {
      $val = $data;
    }
    echo(" <script>console.log('PHP: ". $val ."')</script>");
  }
?>

<html>
  <head>
    <title>PHP Tester</title>
    <style>
     .row { padding: 50px; }
     .image { margin-top: 30px; padding-right: 20px; }
    </style>
  </head>
  <body>
    <div id="heading">
      <?php include 'heading.php' ?>
    </div>
    <?php

      /***  Form Control ***/
      $post_data = array();

      if (isset($_POST['submit'])) {
        foreach($_POST as $data) {
          if (isset($data)) {
            array_push($post_data, $data);
          }
        }

        array_pop($post_data);
        list($question_one, $question_two, $question_three) = $post_data;
	$questions = array("question_one" => $question_one,
			   "question_two" => $question_two,
			   "question_three" => $question_three);

        foreach($questions as $k => $v) {
	  if (!is_null($v) && $v != "") {
	    if (!file_exists($k)){
	      $file = fopen("{$k}.txt", "w") or die ("Unable to open file!");
	      fwrite($file, $v);
	      fclose($file);
	    } else {
	      file_put_contents($k, $v);
	    }
	  }
        }
      }

    ?>
    <form method="post" action="">
      <div class="row">
        <h2>Question 1</h2>
        <a class="image" href="https://www.codeabbey.com/index/task_view/sum-in-loop" target="_blank">
          <img src="https://codeabbey.github.io/data/sum_in_loop.gif">
        </a>
        <textarea name="qOne" rows="10" cols="70"></textarea>
      </div>
      <div class="row">
        <h2>Question 2</h2>
        <a class="image" href="https://www.codeabbey.com/index/task_view/sums-in-loop" target="_blank">
          <img src="https://codeabbey.github.io/data/sums_in_loop.gif">
        </a>
        <textarea name="qTwo" rows="10" cols="70"></textarea>
      </div>
      <div class="row">
        <h2>Question 3</h2>
        <a class="image" href="https://www.codeabbey.com/index/task_view/sums-in-loop" target="_blank">
          <img src="https://codeabbey.github.io/data/min_of_two.gif">
        </a>
        <textarea name="qThree" rows="10" cols="70"></textarea>
      </div>
      <div class="row">
        <input type="submit" name="submit" value="Submit"/>
      </div>
    </form>
    <?php
      <div style="background-color: yellow;">
        echo $slugify->slugify('Hello World!', '_'); // hello_world
      </div>
    ?>
    <?php

      /*** Debugging ***/
      if (isset($questions)) {
          js_log($questions);
      }

    ?>
    <script>
      /*** Do not resubmit form on refresh ***/	
      if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
      }
    </script>
  </body>
</html>
